import io
import pandas
from Bio import Entrez
from Bio import SeqIO
from urllib.error import HTTPError
import time

# Imports for logging prints
import datetime
import sys

# Please stick to the NCBI fair use guidelines
# regarding request numbers, request times, and contact information
# http://biopython.org/DIST/docs/tutorial/Tutorial.html#htoc117

## Initialize log file ##
log_file_name = '{program_name} log {timestamp}.txt'.format(program_name='ncbiCrawler', 
    timestamp=datetime.datetime.now().strftime("%Y-%m-%d %H-%M-%S"))
# Write all prints to log file in working directory
sys.stdout = open('./' + log_file_name, 'w')
# Exceptions will also be written to same file, 
# without overwriting stdout outputs
sys.stderr = sys.stdout

## Entrez setup ##
# Email mandatory according to fair use guidelines
# Please put your email in an external plain text file and read it in here
with open('../NCBI Entrez Email.txt') as f:
    entrez_email = f.readline().strip()
print('Using email:', entrez_email)
Entrez.email = entrez_email
# You can get an API key from your NCBI account's settings page
with open('../NCBI API Key.txt') as f:
    api_key = f.readline().strip()
print('Using API key:', api_key)
Entrez.api_key = api_key
entrez_db_name = 'protein'

# Load output from bacdive crawler to iterate over
df_bacdive_organisms_to_temperatures = pandas.read_json('df_bacdive.json')
df_bacdive_organisms_to_temperatures = df_bacdive_organisms_to_temperatures.sort_index()

### Build protein to BacDive organism dataset ###
# Load intermediate result of NCBI crawler
df_refseq_proteins_to_bacdive_organisms_old = pandas.read_csv('df_refseq_proteins_to_bacdive_organisms.csv', index_col=0)

already_requested_ncbi_tax_ids = set(list(df_refseq_proteins_to_bacdive_organisms_old['ncbi_tax_id'].unique()))
last_bacdive_id = max(df_refseq_proteins_to_bacdive_organisms_old['bacdive_id'].unique())
start_index = max(list(df_refseq_proteins_to_bacdive_organisms_old.index)) + 1
save_filename = 'df_refseq_proteins_to_bacdive_organisms_start_index_{}.csv'.format(start_index)

del df_refseq_proteins_to_bacdive_organisms_old

df_refseq_proteins_to_bacdive_organisms = pandas.DataFrame(columns=['refseq_id', 'protein_seq', 'ncbi_tax_id', 'bacdive_id'])

for index, entry in df_bacdive_organisms_to_temperatures.iterrows():
    new_df_entries = []

    # Write prints to log
    sys.stdout.flush()

    bacdive_id = entry['bacdive_id']
    print('bacdive_id:', bacdive_id)
    if bacdive_id <= last_bacdive_id:
        print('BacDive ID', bacdive_id, 'already requested')
        continue

    is_thermophile = True if entry['t_conditions'] in ['thermophilic', 'hyperthermophilic'] else False

    ncbi_tax_ids = entry['ncbi_tax_ids']

    # Skip rows without any NCBI tax IDs
    if ncbi_tax_ids == None: 
        print('No tax IDs for this organism')
        continue 
    
    # Sometimes NCBI tax IDs link to trees of organisms rather than directly to organisms
    # Because of that we collected all NCBI tax IDs connected to each organism on BacDive
    # and we try to find out which of them directly lead to an organism
    for ncbi_tax_id in ncbi_tax_ids:
        # Write prints to log
        sys.stdout.flush()

        # Keep track of the NCBI tax IDs we have already requested
        # because they appear multiple times in the dataset,
        # but the query results are the same of course
        if ncbi_tax_id in already_requested_ncbi_tax_ids: 
            print('Already requested tax ID', ncbi_tax_id)
            continue
        else:
            already_requested_ncbi_tax_ids.add(ncbi_tax_id)

        print('ncbi_tax_id:', ncbi_tax_id)

        ## Get protein UIDs for this ncbi tax id ##

        entrez_query = "refseq[filter] AND txid{}".format(ncbi_tax_id)
        entrez_query_handle = Entrez.esearch(db=entrez_db_name, term=entrez_query, usehistory="y")
        response = Entrez.read(entrez_query_handle)
        entrez_query_handle.close()

        ## Process response ##
        try:
            # Extract field IdList from XML response containing protein GI numbers
            webenv = response['WebEnv']
            query_key = response['QueryKey']
            count = int(response["Count"])
        except:
            # Check if response is empty (no RefSeq proteins found for this organism)
            # and continue with next ncbi tax id for this organism
            print('Error processing XML response, continuing with next ncbi tax id...')
            continue
        
        # Avoid trying to batch-download no proteins at all
        if count <= 0:
            print('No proteins found, continuing with next ncbi tax id...')
            continue

        print(count, 'proteins found, downloading', min(count, 1000) if not is_thermophile else count, '...')

        # Fetch all proteins specified in uid_list as FASTA from Entrez
        # by using batch download and simple server error detection, see:
        # http://biopython.org/DIST/docs/tutorial/Tutorial.html#sec:entrez-webenv
        batch_size = 100
        for start in range(0, min(count, 1000) if not is_thermophile else count, batch_size):
            # Write prints to log
            sys.stdout.flush()

            end = min(count, start+batch_size)
            print("Going to download record {} to {}".format(start+1, end))
            
            attempt = 0
            while attempt < 3:
                attempt += 1
                try:
                    fetch_handle = Entrez.efetch(db=entrez_db_name, rettype='fasta', retmode='text',
                        webenv=webenv, query_key=query_key, retstart=start, retmax=batch_size)
                except HTTPError as err:
                    if 500 <= err.code <= 599:
                        print('Received error from server {}\nAttempt {} of 3'.format(err, attempt))
                        time.sleep(15)
                    else:
                        raise

            protein_catalog = fetch_handle.read()
            fetch_handle.close()

            # Load protein catalog string into StringIO memory file
            # as preparation for SeqIO.parse() method which expects a file
            # WARNING: File must not be closed because SeqIO.parse() will somehow
            # try to continue working on the closed file, unresolved error
            f = io.StringIO(protein_catalog)

            # Parse protein catalog FASTA with Biopython
            parsed_protein_catalog = SeqIO.parse(f, 'fasta')

            # Add each protein from parsed protein catalog to dataframe
            for protein_record in parsed_protein_catalog:
                new_df_entries.append({'refseq_id': protein_record.id,
                    'protein_seq': str(protein_record.seq), 
                    'ncbi_tax_id': ncbi_tax_id, 'bacdive_id': bacdive_id})

        # If we found one protein catalog for this organism already, we don't need to make
        # further requests and therefore won't look at the other ncbi tax ids for this organism
        # If we didn't we would have skipped to the next iteration earlier already
        break

    # After each completed organism, add new data to dataframe and save it
    if len(new_df_entries) != 0:
        df_new_df_entries = pandas.DataFrame(new_df_entries, 
            columns=['refseq_id', 'protein_seq', 'ncbi_tax_id', 'bacdive_id'], 
            index=pandas.RangeIndex(start=start_index, stop=start_index + len(new_df_entries)))
        
        df_refseq_proteins_to_bacdive_organisms = pandas.concat([df_refseq_proteins_to_bacdive_organisms, df_new_df_entries])

        df_refseq_proteins_to_bacdive_organisms.to_csv(save_filename)

        start_index += len(new_df_entries)

