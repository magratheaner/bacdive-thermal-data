import pandas
import requests
import time
import re

# Imports for logging prints
import datetime
import sys

## Initialize log file ##
log_file_name = '{program_name} log {timestamp}.txt'.format(program_name='bacDiveCrawler', 
    timestamp=datetime.datetime.now().strftime("%Y-%m-%d %H-%M-%S"))
# Write all prints to log file in working directory
sys.stdout = open('./' + log_file_name, 'w')
# Exceptions will also be written to same file, 
# without overwriting stdout outputs
sys.stderr = sys.stdout

# Put your BacDive API authentication in a plain text file, but please don't add it to the repo
# username
# password

with open('../BacDive API Auth.txt') as f:
    auth_user = f.readline().strip()
    auth_pass = f.readline().strip()

# Precompiled list of all BacDive IDs with temperature information
with open('resources/all_organisms_with_temperature_information_list.txt') as f:
    bacdive_ids = [int(line.strip()) for line in f]

### Build BacDive organism to temperature dataset ###

df_bacdive_organisms_to_temperatures = pandas.DataFrame(columns=['bacdive_id', 'ncbi_tax_ids', 't_conditions', 't_growth', 't_optimum', 't_maximum', 't_minimum'])
df_bacdive_organisms_to_temperatures = df_bacdive_organisms_to_temperatures.set_index('bacdive_id')

iterations = 0
for bacdive_id in bacdive_ids:
    # Back off of server to avoid DDoS
    time.sleep(0.2)
    if iterations % 100 == 0:
        time.sleep(10)

    iterations += 1
    new_df_entries = []
    print('Fetching data on organism', bacdive_id)

    # Write prints to log
    if iterations % 10 == 0:
        sys.stdout.flush()

    ncbi_tax_ids_set = set()

    ## Request information about organism ##
    parsed_response = requests.get('https://bacdive.dsmz.de/api/bacdive/bacdive_id/' + str(bacdive_id) + '/?format=json', auth=(auth_user, auth_pass)).json()

    ## Process response ##

    # If no sequencing projects available for organism skip to next one
    try:
        sequencing_projects = parsed_response['molecular_biology']['sequence']

        # Collect NCBI taxonomy accessions
        for entry in sequencing_projects:
            ncbi_tax_id = entry['NCBI_tax_ID']
            if ncbi_tax_id != None:
                ncbi_tax_ids_set.add(ncbi_tax_id)
    except:
        print('no sequencing projects on organism', bacdive_id)

    #new_df_entries.append({'bacdive_id': bacdive_id, 'ncbi_tax_ids': ncbi_tax_ids_set if ncbi_tax_ids_set else None})

    # Collect temperature information
    try:
        t_tests = parsed_response['culture_growth_condition']['culture_temp']
    except:
        print('no temperature information on organism', bacdive_id, '(this should not happen)')
        print('or BacDive authentication failed (much more probable)')
        break

    t_test_types = ['growth', 'optimum', 'maximum', 'minimum']
    t_sets = {t_test_type: None for t_test_type in t_test_types}
    t_conditions = None

    # Each t_test dictionary will contain one temperature information
    # e.g. on optimum or growth temperature
    for t_test in t_tests:
        # Extract the relevant fields from the entry
        t_test_type = t_test['test_type']
        t_temp = t_test['temp']
        t_ability = t_test['ability']
        
        # Skip tests that are negative or don't actually contain temperature information
        if t_ability != 'positive' or t_temp == None:
            print('Found useless test: ability', t_ability, 'temp_range', t_temp)
            continue

        if t_test['temperature_range'] != None:
            t_conditions = t_test['temperature_range']
        
        # Match the temperature field of the test against this regex
        # (accounts for arbitrary amounts of whitespace and arbitrary two numbers)
        # If a given information doesn't match, we will skip the test
        matches = re.search('\A\s*(?P<t_lower_bound>[-]{0,1}\d+(\.\d+){0,1})\s*-\s*(?P<t_upper_bound>[-]{0,1}\d+(\.\d+){0,1})\s*\Z', t_temp)
    
        if matches == None:

            matches = re.search('\A\s*(?P<t_single_temp>[-]{0,1}\d+(\.\d+){0,1})\s*\Z', t_temp)

            if matches == None:
                print('Regex match for temp information failed, input string:', t_temp)
                continue

            t_range = float(matches.group('t_single_temp'))

        else:
            t_lower_bound = float(matches.group('t_lower_bound'))
            t_upper_bound = float(matches.group('t_upper_bound'))
            
            if t_upper_bound < t_lower_bound:
                print('Temp information illogical (i.e. t_upper_bound < t_lower_bound):', t_temp)
                continue

            t_range = [t_lower_bound, t_upper_bound]
        
        # Insert temperature range tuple into the temperature set (check if empty) for this organism
        try:
            t_sets[t_test_type].append(t_range)
        except AttributeError:
            t_sets[t_test_type] = [t_range]
        
    new_df_entries.append({'bacdive_id': bacdive_id, 
        'ncbi_tax_ids': ncbi_tax_ids_set if ncbi_tax_ids_set else None, 't_conditions': t_conditions,
        't_growth': t_sets['growth'], 't_optimum': t_sets['optimum'], 
        't_maximum': t_sets['maximum'], 't_minimum': t_sets['minimum']})

    # Add new entries to existing DataFrame
    # sorry for the index gore, pandas to_json() was very needy in that regard
    if len(new_df_entries) != 0:
        df_new_df_entries = pandas.DataFrame(new_df_entries, columns=['bacdive_id', 'ncbi_tax_ids', 't_conditions', 't_growth', 't_optimum', 't_maximum', 't_minimum'])
        df_new_df_entries = df_new_df_entries.set_index('bacdive_id')
        df_bacdive_organisms_to_temperatures = pandas.concat([df_bacdive_organisms_to_temperatures, df_new_df_entries])

    print('Entry accepted')

    # Save a checkpoint of the dataframe every 100 organisms
    if iterations % 100 == 0:
        df_bacdive_organisms_to_temperatures.to_json('df_bacdive_organisms_to_temperatures.json')

# Save the complete dataframe after all organisms were iterated over
df_bacdive_organisms_to_temperatures.to_csv('df_bacdive_organisms_to_temperatures.csv')
df_bacdive_organisms_to_temperatures.to_json('df_bacdive_organisms_to_temperatures.json')